<html>
<body>
	<!-- <h1>edit form</h1> -->
<?php

require ("functions.php");
session_start();
auth();
$connect = dbConnect();

$id = $_REQUEST['id'];
//echo $id;

$editform_data_sql = "SELECT * FROM `students` WHERE `students`.`id` = '$id'";
//var_dump($editform_data_sql);

$edit_data = $connect->query($editform_data_sql);
//var_dump($edit_data);

$data = $edit_data->fetch_assoc();
//print_r($data);
//echo ($data['student_name']);
//echo ($data['phone']);

?>
	
	<div align="center">
        <form action="edit_action.php" method="post" enctype="multipart/form-data">
            <div style="font-size: 12px"><b> Edit Data </b></div>
			<input type="hidden" name="id" value="<?php echo ($data["id"]); ?>" required/>	
                <div class="sameline">
					<label for="name"><b> Name: </b></label>
					<input type="text" placeholder="enter name" name="u_name" value="<?php echo ($data['student_name']);?>" autocomplete="off" required/>
				</div><br>
                <div class="sameline">
					<label for="phn"><b> Contact: </b></label>
					<input type="text" placeholder="enter contact" name="u_phn" value="<?php echo ($data['phone']); ?>" autocomplete="off" required/>
                </div><br>
				
                <div>
                    <input type="submit" value="Save"/>
                    <input type="reset" value="Clear Form"/>
                    <br>
                </div>
			</div>
        </form>
	</div>
</body>
</html>