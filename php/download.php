<?php
//echo "download.php file";
/** database connection **/
require ("functions.php");
session_start();
auth();
$con = dbConnect();
/** end database connection **/

/** hard code **/
/*
//data to be downloaded
$data = [
			["sr.no.", "name", "conact"],
			["1", "abc", "325"],
			["2", "xyz", "976"],
];
//echo "<pre>";
//print_r($data);

$filename = 'student.csv'; //file creation

$file = fopen($filename, "w"); //open file in write mode

//puting array in file
foreach($data as $row){
		fputcsv($file, $row);
}


fclose($file); //close  file

//download file
header("Content-Discription: File Transfer");
header("Content-Disposition: attachment; filename=".$filename);
header("Content-Type: application/csv; ");

readfile($filename);

//deleting file
unlink($filename);
exit();
*/
/** end hard code **/

/** dynamic code **/

//getting download data from database
$select_sql = "SELECT * FROM `students`";
	
//	echo $select_sql;

$res = $con->query($select_sql);
//var_dump($res);
//$row_count = mysqli_num_rows($res); //count the no of rows 
//echo $row_count;

$filename = 'comp_students.csv'; //file creation

$file = fopen($filename, "w"); //open file in write mode

fputcsv($file, ["id", "name", "contact"]);

if (mysqli_num_rows($res) > 0){
	
	while($row = $res->fetch_row()){
		
		fputcsv($file, $row);
	}
}

fclose($file); //close  file

//download
header("Content-Discription: File Transfer");
header("Content-Disposition: attachment; filename=".$filename);
header("Content-Type: application/csv; ");

readfile($filename);

//deleting file
unlink($filename);
exit();

/** end dynamic code **/

?>